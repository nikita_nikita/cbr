package com.nikita.cbrusd.ui.screens.asp

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import com.nikita.cbrusd.R
import com.nikita.cbrusd.databinding.FragmentMainBinding

@AndroidEntryPoint
class MainFragment : Fragment(R.layout.fragment_main) {

    private val viewModel: MainViewModel by viewModels()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val binding = FragmentMainBinding.bind(view)

        viewModel.loading.observe(viewLifecycleOwner) {
            binding.content.displayedChild = 0
        }

        viewModel.result.observe(viewLifecycleOwner) {
            binding.content.displayedChild = 1
            binding.recyclerView.adapter = MainAdapter(it)
        }
        fetch()
    }

    private fun fetch() {
        viewModel.fetch()
    }
}