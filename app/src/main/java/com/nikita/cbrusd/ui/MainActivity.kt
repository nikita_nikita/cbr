package com.nikita.cbrusd.ui

import androidx.appcompat.app.AppCompatActivity
import com.nikita.cbrusd.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity(R.layout.activity_main)