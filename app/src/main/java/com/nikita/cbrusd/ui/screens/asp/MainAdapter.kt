package com.nikita.cbrusd.ui.screens.asp


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nikita.cbrusd.api.ASP
import com.nikita.cbrusd.databinding.ItemMainBinding

class MainAdapter(private val aspList: List<ASP>) : RecyclerView.Adapter<ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        ItemMainBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun getItemCount() = aspList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(aspList[position])
    }
}

class ViewHolder(private val binding: ItemMainBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(asp: ASP) {
        binding.date.text = asp.date
        binding.value.text = asp.value.toString()
    }
}