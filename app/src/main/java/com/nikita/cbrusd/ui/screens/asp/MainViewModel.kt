package com.nikita.cbrusd.ui.screens.asp

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import com.nikita.cbrusd.api.ASP
import com.nikita.cbrusd.api.ASPList
import com.nikita.cbrusd.api.CBRApi
import com.nikita.cbrusd.device.Notification
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val api: CBRApi,
                                            private val notificationManager: Notification
) : ViewModel() {

    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean>
        get() = _loading

    val result: LiveData<List<ASP>>
        get() = _result
    private val _result = MutableLiveData<List<ASP>>()

    val triggerValue:Double = "72.50".toDouble()
    private val  VAL_NM_RQ: String = "R01235"

    val  date1 = myDateFormat(LocalDate.now().withDayOfMonth(1))
    val  date2 =  myDateFormat(LocalDate.now())

    private fun onLoaded(list: List<ASP>){
        _result.postValue(list)
        checkList(list)
    }

    fun checkList(list: List<ASP>) {
        list.forEach() {
            if (it.value.replace(",", ".").toDouble() > triggerValue) {
                notificationManager.show(
                    "Внимание",
                    "Замечено значение больше $triggerValue"
                )
                return
            }
        }
    }



    fun myDateFormat(date :LocalDate) : String = DateTimeFormatter.ofPattern("dd/MM/yyyy").format(date)

    fun fetch() {
        val callSync: Call<ASPList> = api.getDynamicASP(date1,date2,VAL_NM_RQ)
        callSync.enqueue(object : Callback<ASPList?> {
            override fun onResponse(call: Call<ASPList?>?, response: Response<ASPList?>) {
                when (response.isSuccessful) {
                    true -> onLoaded(response.body()!!.items)
                    false ->  println("Request Error :: " + response.errorBody())
                }
            }
            override fun onFailure(call: Call<ASPList?>?, t: Throwable) =
                println("Network Error :: " + t.localizedMessage)

        })
    }



}


