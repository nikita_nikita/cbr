package com.nikita.cbrusd.device

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.nikita.cbrusd.ui.MainActivity
import com.nikita.cbrusd.R
import javax.inject.Inject


class Notification @Inject constructor(
    private val context: Context,
    private val notificationManager: NotificationManager,
    private val notificationBuilder: NotificationCompat.Builder
){
    init{
        createNotificationChanel()
    }
    companion object{
        const val CHANNEL_ID = "Re"
        const val notification_id =1
    }

    fun show(contentTitle:String, contentText: String) {
        val builder = createNotification()
        builder.setContentTitle(contentTitle)
            .setContentText(contentText)
        with(NotificationManagerCompat.from(context)){
            notify(notification_id, builder.build())
        }
    }

    private fun pendingIntent(): PendingIntent? {
        val intent = Intent(context, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        return  PendingIntent.getActivity(context,0,intent,0)
    }

    private fun createNotification(): NotificationCompat.Builder = notificationBuilder
        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
        .setSmallIcon(R.drawable.ic_launcher_background)
        .setContentIntent(pendingIntent())

    private fun createNotificationChanel() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            val important = NotificationManager.IMPORTANCE_DEFAULT
            val chanel = NotificationChannel(CHANNEL_ID,"DOLLAR",important).apply {
                description= "Trigger"
            }
            notificationManager.createNotificationChannel(chanel)
        }
    }
}