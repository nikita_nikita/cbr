package com.nikita.cbrusd.di

import android.app.NotificationManager
import android.content.Context
import androidx.core.app.NotificationCompat
import com.tickaroo.tikxml.TikXml
import com.tickaroo.tikxml.retrofit.TikXmlConverterFactory
import dagger.Module
import dagger.Provides
import dagger.Reusable
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import com.nikita.cbrusd.api.CBRApi
import com.nikita.cbrusd.device.Notification
import com.nikita.cbrusd.device.Notification.Companion.CHANNEL_ID
import dagger.hilt.components.SingletonComponent
import okhttp3.HttpUrl
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.create
import java.util.concurrent.TimeUnit


@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Reusable
    fun provideApiHost() = "https://www.cbr.ru".toHttpUrlOrNull()!!

    @Provides
    @Reusable
    fun provideClient() = OkHttpClient.Builder()
        .connectTimeout(120, TimeUnit.SECONDS)
        .readTimeout(120, TimeUnit.SECONDS)
        .writeTimeout(120, TimeUnit.SECONDS)
        .build();

    @Provides
    @Reusable
    fun provideXmlConverterFactory(): TikXmlConverterFactory = TikXmlConverterFactory.create(
        TikXml.Builder()
            .exceptionOnUnreadXml(false)
            .build()
    )

    @Provides
    @Reusable
    fun provideRetrofit(provideClient: OkHttpClient, provideApiHost: HttpUrl,
                        provideXmlConverterFactory: TikXmlConverterFactory
    ): Retrofit = Retrofit.Builder()
        .client(provideClient)
        .baseUrl(provideApiHost)
        .addConverterFactory(provideXmlConverterFactory)
        .build()

    @Provides
    @Reusable
    fun provideCBRApi(retrofit: Retrofit) = retrofit.create<CBRApi>()
}

@Module
@InstallIn(SingletonComponent::class)
object NotificationModule {

    @Reusable
    @Provides
    fun provideNotificationManager(@ApplicationContext context: Context): NotificationManager =
        context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager


    @Reusable
    @Provides
    fun provideNotificationCompat(@ApplicationContext context: Context): NotificationCompat.Builder =
        NotificationCompat.Builder(context, CHANNEL_ID)


    @Reusable
    @Provides
    fun provideNotificationUtil(
        @ApplicationContext context: Context,
        notificationManager: NotificationManager,
        notificationBuilder: NotificationCompat.Builder
    ): Notification = Notification(context, notificationManager, notificationBuilder)

}