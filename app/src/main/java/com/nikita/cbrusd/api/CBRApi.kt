package com.nikita.cbrusd.api


import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


public interface CBRApi {

    @GET("/scripts/XML_dynamic.asp")
    fun getDynamicASP(@Query ("date_req1") date_from: String,
                      @Query ("date_req2") date_to: String,
                      @Query ("VAL_NM_RQ") ticker: String
    ): Call<ASPList>
}