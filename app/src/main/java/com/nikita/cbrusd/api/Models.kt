package com.nikita.cbrusd.api

import com.tickaroo.tikxml.annotation.Attribute
import com.tickaroo.tikxml.annotation.Element
import com.tickaroo.tikxml.annotation.PropertyElement
import com.tickaroo.tikxml.annotation.Xml



@Xml(name = "ValCurs")
data class ASPList(
    @Element(name = "Record") val items:List<ASP>
)
@Xml(name = "Record")
data class ASP (
    @Attribute(name = "Date")
    var date: String,
    @PropertyElement(name = "Value")
    var value: String
)


